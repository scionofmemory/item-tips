# Item Tips

Shows helpful UI tips about items when you are in proximity to them. This included coolness, curse, synergies, and a short description.
Adds extended description to the ammonomicon entry for the item.

Additional features:
  - load tips from external files
  - cooldown counter for active items

## External tip files

External tip files are json formatted files. The ItemTips will search all mod folders for files matching the pattern "itemtips-*.tip".

The format of the files:

```json
{
    // note metadata is optional.
    "metadata": {
        "name": "My Item Tips",
        "url": "url to find updates",
        "version": "1.0.0"
    },
    "items": {
        // Key is the console name used in the mtg api console.
        "prefix:item_name": {
            "name": "Item Name", // item name is optional.
            "notes": "A description of the item."
        }
    },
    "synergies": {
        // Key is the AdvancedSynergyEntry.NameKey of the synergy.
        "Synergy Name": {
            "notes": "A description of the synergy."
        }
    }
}
```

The console command `itemtips generate` will generate a sample file for all loaded items and synergies. To show only items for
a specific prefix use `itemtips generate my_prefix`.

To load external files, either include tip files in a mod or set the "External Data Path" configuration value for the ItemTips mod
to a directory to load from.

## Special Thanks
* Dallan for making the icon.

---

## Changelog

* 2.0.0 - Initial BepInEx port.
* 2.0.1 - Fix missing internal data.
* 2.1.0 - Support external tip files.
* 2.2.0 - Label positioning exposed to the BepInEx configuration file.
