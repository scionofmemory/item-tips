﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ItemTipsMod;
using NUnit.Framework;

namespace ModTests
{
    class LoaderTest
    {
        [Test]
        public void BeforeTest()
        {
            Loader.ThrowIfException = true;
        }

        [Test]
        public void BasicLoadingTest()
        {
            var data = Loader.LoadItemData();
            Assert.That(data.Pickups.Count, Is.GreaterThan(0), "items");
            Assert.That(data.Synergies.Count, Is.GreaterThan(0), "synergies");
        }

        [Test]
        [TestCase(0, "Magic Lamp")]
        [TestCase(115, "Ballot")]
        public void SomeBasicItemNamesLoaded(int id, string name)
        {
            var data = Loader.LoadItemData();
            var entry = data.Pickups[id];
            Assert.AreEqual(name, entry.Name);
        }

        [Test]
        [TestCase(467)]
        [TestCase(468)]
        [TestCase(469)]
        [TestCase(470)]
        [TestCase(471)]
        public void MasterRoundTest(int id)
        {
            var data = Loader.LoadItemData();
            var entry = data.Pickups[id];
            Assert.AreEqual("Master Round", entry.Name);
        }

        [TestCaseSource(nameof(GetItemCases))]
        public void HasItemNameAndNotes(ItemData item)
        {
            Assert.That(item.Name, Is.Not.Null.And.Not.Empty, "missing name");
            Assert.That(item.Notes, Is.Not.Null.And.Not.Empty, "missing notes");
        }

        [TestCaseSource(nameof(GetSynergyCases))]
        public void HasSynergyNameAndNotes(SynergyData synergy)
        {
            Assert.That(synergy.Name, Is.Not.Null.And.Not.Empty, "missing name");
            Assert.That(synergy.Effect, Is.Not.Null.And.Not.Empty, "missing notes");
        }

        [Test]
        public void UniqueItemIds()
        {
            var data = Loader.GetInternalDataSet();
            var hashSet = new HashSet<int>();
            var failed = new List<ItemData>();
            foreach (var item in data.Items)
            {
                if (item.Id != null)
                {
                    bool added = hashSet.Add(item.Id.GetValueOrDefault());
                    if (!added)
                    {
                        failed.Add(item);
                    }
                }
                else
                {
                    foreach (var id in item.Ids)
                    {
                        bool added = hashSet.Add(id);
                        if (!added)
                        {
                            failed.Add(item);
                        }
                    }
                }
            }

            if (failed.Count > 0)
            {
                string items = string.Join(Environment.NewLine, failed.Select(i => $"{i.Id},{i.Name}").ToArray());
                Assert.Fail($"Duplicate items: {items}");
            }
        }

        [Test]
        public void UniqueSynergyIds()
        {
            var data = Loader.GetInternalDataSet();
            var hashSet = new HashSet<string>();
            var failed = new List<SynergyData>();
            foreach (var synergy in data.Synergies)
            {
                bool added = hashSet.Add(synergy.Key);
                if (!added)
                {
                    failed.Add(synergy);
                }
            }

            if (failed.Count > 0)
            {
                string items = string.Join(Environment.NewLine, failed.Select(i => $"{i.Key},{i.Name}").ToArray());
                Assert.Fail($"Duplicate items: {items}");
            }
        }

        [Test]
        public void LoadExternalDataAddsPeriodToNotes()
        {
            var testData = new ExternalDataSet()
            {
                Metadata = new ExternalMetadata()
                {
                    Name = "my mod",
                    Version = "1.2.3",
                    Url = "http://modworkshop.net"
                },
                Items = new Dictionary<string, ExternalPickupData>()
                {
                    { "mymod:item1", new ExternalPickupData() { Name = "Item1", Notes = "My notes" } },
                    { "mymod:item2", new ExternalPickupData() { Name = "Item2", Notes = "My notes 2." } }
                },
                Synergies = new Dictionary<string, ExternalSynergyData>()
                {
                    { "my synergy", new ExternalSynergyData() { Name = "my synergy", Notes = "synergy description" } }
                },
            };

            string testDir = InitializeExternalData(testData);

            var externalData = Loader.LoadExternalTips(testDir);
            Assert.AreEqual(2, externalData.Pickups.Count);
            Assert.AreEqual("My notes.", externalData.Pickups["mymod:item1"].Notes);
            Assert.AreEqual("My notes 2.", externalData.Pickups["mymod:item2"].Notes);
            Assert.AreEqual(1, externalData.Synergies.Count);
            Assert.AreEqual("synergy description.", externalData.Synergies["my synergy"].Notes);
        }

        public static string InitializeExternalData(ExternalDataSet externalData)
        {
            string extDir = Path.Combine(TestContext.CurrentContext.TestDirectory, "externaldatatest");
            if (Directory.Exists(extDir))
            {
                Directory.Delete(extDir, true);
            }

            Directory.CreateDirectory(extDir);


            string testJsonFile = Path.Combine(extDir, "test.json");
            JsonHelper.SerializeToFile(testJsonFile, externalData);

            return extDir;
        }

        public static IEnumerable<TestCaseData> GetItemCases()
        {
            var data = Loader.LoadItemData();
            foreach (var kvp in data.Pickups)
            {
                yield return new TestCaseData(kvp.Value)
                {
                    TestName = $"ItemHasNameAndNotes({kvp.Key},{kvp.Value?.Name})"
                };
            }
        }

        public static IEnumerable<TestCaseData> GetSynergyCases()
        {
            var data = Loader.LoadItemData();
            foreach (var kvp in data.Synergies)
            {
                yield return new TestCaseData(kvp.Value)
                {
                    TestName = $"SynergyHasNameAndNotes({kvp.Key},{kvp.Value?.Name})"
                };
            }
        }
    }
}
