﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using ModReflectionHelper = ItemTipsMod.ReflectionHelper;

namespace ModTests
{
    class ReflectionHelperTest
    {
        public int ActionZeroCalled = 0;

        public int ActionOneCalled = 0;

        [Test]
        public void CreateInstanceDelegateActionZeroArg()
        {
            var action = (Action)ModReflectionHelper.CreateInstanceDelegate(this, nameof(ActionZero));
            action();
            Assert.AreEqual(1, ActionZeroCalled);
        }

        [Test]
        public void CreateInstanceDelegateActionOneArg()
        {
            var action = (Action<int>)ModReflectionHelper.CreateInstanceDelegate(this, nameof(ActionOne));
            action(1);
            Assert.AreEqual(1, ActionOneCalled);
        }

        private void ActionZero()
        {
            ActionZeroCalled++;
        }

        private void ActionOne(int arg1)
        {
            ActionOneCalled++;
        }
    }
}
