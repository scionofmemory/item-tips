# Item Tips

Item Tips mod for Enter the Gungeon

## Development environment

Visual Studio 2019 on Windows.

## Prequisites to build

1. Have Enter the Gungeon installed with BepInEx installed. A mod manager like [r2modman](https://github.com/ebkr/r2modmanPlus) can be helpful.

## Build setup

1. Run `.\build.ps1`. This will restore any packages and run some tests.

## Building and packaging

To build and create release zip:

```powershell
.\build.ps1 --target=Package
```

This will generate a ItemTipsMod_{version}.zip in the `.\publish` folder.