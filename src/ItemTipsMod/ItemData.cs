﻿namespace ItemTipsMod
{
    public class ItemData
    {
        public string Name;

        // the pickup id
        public int? Id;

        // could be multiple ids for notes or master round.
        public int[] Ids;

        public string Notes;

        public SynergyData[] Synergies = ArrayHelper<SynergyData>.Empty;

        public SourceMetadata SourceMetadata;
    }

    public class SourceMetadata
    {
        public string Name;

        public string Url;

        public string Version;

        public static readonly SourceMetadata InternalSource = new SourceMetadata() { Name = "internal" };

        public override string ToString()
        {
            return $"SourceMetadata:{Name}";
        }
    }
}

