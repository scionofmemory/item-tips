﻿using System;
using System.Collections.Generic;
using Gungeon;

namespace ItemTipsMod
{
    public class PickupAndSynergyDataCache
    {
        public Dictionary<int, ItemData> Pickups = new Dictionary<int, ItemData>();

        public Dictionary<string, SynergyData> Synergies = new Dictionary<string, SynergyData>();

        public int LoadExternalDirectory(string directory)
        {
            int loaded = 0;
            try
            {
                if (string.IsNullOrEmpty(directory))
                {
                    StaticLogger.LogDebug("Skipped loading external files because directory was not set.");
                    return 0;
                }

                var externalCache = Loader.LoadExternalTips(directory);
                if (externalCache.Pickups.Count == 0 && externalCache.Synergies.Count == 0)
                {
                    return loaded;
                }

                int skipped = 0;
                foreach (var kvp in externalCache.Pickups)
                {
                    string id = kvp.Key;
                    var data = kvp.Value;

                    if (string.IsNullOrEmpty(data.Notes))
                    {
                        StaticLogger.LogDebug($"Skipped item: {id}, no 'Notes' set.");
                        skipped++;
                        continue;
                    }

                    if (!Game.Items.ContainsID(id))
                    {
                        StaticLogger.LogDebug($"Skipped item: {id}, game has not loaded it.");
                        skipped++;
                        continue;
                    }

                    var pickup = Game.Items[id];
                    if (pickup.PickupObjectId < 0)
                    {
                        StaticLogger.LogDebug($"Skipped item: {id}, pickup has not been assigned an id.");
                        skipped++;
                        continue;
                    }


                    SourceMetadata itemMetadata = null;
                    if (Pickups.TryGetValue(pickup.PickupObjectId, out var existingData))
                    {
                        if (existingData.SourceMetadata == SourceMetadata.InternalSource)
                        {
                            itemMetadata = SourceMetadata.InternalSource;
                        }
                        else
                        {
                            if (data.SourceMetadata != null)
                            {
                                itemMetadata = new SourceMetadata()
                                {
                                    Name = data.SourceMetadata.Name,
                                    Version = data.SourceMetadata.Version,
                                    Url = data.SourceMetadata.Url,
                                };
                            }
                        }
                    }
                    else
                    {
                        itemMetadata = new SourceMetadata()
                        {
                            Name = data.SourceMetadata.Name,
                            Version = data.SourceMetadata.Version,
                            Url = data.SourceMetadata.Url,
                        };
                    }

                    Pickups[pickup.PickupObjectId] = new ItemData()
                    {
                        Name = data.Name ?? id,
                        Notes = data.Notes,
                        SourceMetadata = itemMetadata
                    };

                    loaded++;
                }

                foreach (var kvp in externalCache.Synergies)
                {
                    string id = kvp.Key;
                    var data = kvp.Value;

                    if (string.IsNullOrEmpty(data.Notes))
                    {
                        StaticLogger.LogDebug($"Skipped synergy: {id}, no 'Notes' set.");
                        skipped++;
                        continue;
                    }

                    SourceMetadata synergyMetadata = null;
                    if (Synergies.TryGetValue(id, out var existingData))
                    {
                        if (existingData.SourceMetadata == SourceMetadata.InternalSource)
                        {
                            synergyMetadata = SourceMetadata.InternalSource;
                        }
                        else
                        {
                            if (data.SourceMetadata != null)
                            {
                                synergyMetadata = new SourceMetadata()
                                {
                                    Name = data.SourceMetadata.Name,
                                    Version = data.SourceMetadata.Version,
                                    Url = data.SourceMetadata.Url,
                                };
                            }
                        }
                    }
                    else
                    {
                        synergyMetadata = new SourceMetadata()
                        {
                            Name = data.SourceMetadata.Name,
                            Version = data.SourceMetadata.Version,
                            Url = data.SourceMetadata.Url,
                        };
                    }

                    Synergies[id] = new SynergyData()
                    {
                        Name = data.Name ?? id,
                        Effect = data.Notes,
                        SourceMetadata = synergyMetadata
                    };

                    loaded++;
                }

                StaticLogger.LogDebug($"Loaded external tips:{loaded}, skipped:{skipped}");

                return loaded;
            }
            catch (Exception e)
            {
                StaticLogger.LogError($"Failed to load external data. Error: {e}");
            }

            return loaded;
        }
    }
}
