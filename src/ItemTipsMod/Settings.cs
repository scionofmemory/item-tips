﻿using System;
using UnityEngine;

namespace ItemTipsMod
{
    public class Settings
    {
        public int LineWidth = 40;
        public Vector2 GetSize(int linesHeight)
        {
            return new Vector2(11f * LineWidth, 32f * linesHeight);
        }
    }
}

