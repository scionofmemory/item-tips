﻿using System;
using System.Reflection;
using UnityEngine;

namespace ItemTipsMod
{
    internal class CooldownLabelBehavior : MonoBehaviour
    {
        public static CooldownLabelBehavior Instance;

        public PlayerCooldownLabelCollection labelCollection;

        public bool forceUpdate = false, forceInitialize = false;

        public void UpdateLabelPostion()
        {
            foreach (var playerLabel in labelCollection.cooldownLabelData)
            {
                playerLabel.UpdateLabelPostion();
            }
        }

        public void SetVisible(bool visible)
        {
            labelCollection.ForEach(c => c.cooldownLabel.IsVisible = visible);
        }

        private void Start()
        {
            try
            {
                labelCollection = new PlayerCooldownLabelCollection();
                GameManager.Instance.OnNewLevelFullyLoaded += OnNewLevelFullyLoaded;
                Instance = this;
            }
            catch (Exception e)
            {
                StaticLogger.LogDebug("Error in CooldownLabelBehavior.Start: {0}", e);
            }
        }

        private void OnNewLevelFullyLoaded()
        {
            try
            {
                labelCollection.Clear();
                forceInitialize = true;
                forceUpdate = true;
            }
            catch (Exception e)
            {
                StaticLogger.LogDebug("Error in CooldownLabelBehavior.OnNewLevelFullyLoaded: {0}", e);
            }
        }

        private void Update()
        {
            try
            {
                if (GameManager.Instance.CurrentLevelOverrideState == GameManager.LevelOverrideState.FOYER)
                {
                    return;
                }

                if (GameManager.Instance.IsLoadingLevel)
                {
                    forceUpdate = true;
                    forceInitialize = true;
                    return;
                }

                if (!GameUIRoot.Instance.IsCoreUIVisible())
                {
                    forceUpdate = true;
                    return;
                }

                if (forceInitialize)
                {
                    labelCollection.Initialize(GameManager.Instance.AllPlayers.Length);
                    forceInitialize = false;
                }

                for (int i = 0; i < GameManager.Instance.AllPlayers.Length; i++)
                {
                    try
                    {
                        var player = GameManager.Instance.AllPlayers[i];
                        var playerLabel = labelCollection[i];

                        bool dirty = playerLabel.UpdateBoundState(player);
                        if (dirty | forceUpdate)
                        {
                            forceUpdate = false;
                            playerLabel.UpdateLabelState();
                        }
                    }
                    catch (Exception e)
                    {
                        StaticLogger.LogDebug("Error updating cooldown label for player {0}: {1}", i, e);
                    }
                }
            }
            catch (Exception e)
            {
                StaticLogger.LogDebug("Error in Cooldown behavior Update: {0}", e);
            }
        }
    }

    internal class PlayerCooldownLabelCollection
    {
        private static Func<GameUIRoot, int, GameUIItemController> GetPlayerItemController = GetPlayerItemControllerDelegate();

        public PlayerCooldownLabelData[] cooldownLabelData;

        public PlayerCooldownLabelCollection()
        {
        }

        public PlayerCooldownLabelData this[int index]
        {
            get => cooldownLabelData[index];
        }

        public void ForEach(Action<PlayerCooldownLabelData> action)
        {
            if (cooldownLabelData != null)
            {
                foreach (var labelData in cooldownLabelData)
                {
                    action(labelData);
                }
            }
        }

        public void Initialize(int playerCount)
        {
            StaticLogger.LogDebug("Initializing PlayerCooldownLabelCollection, playerCount: {0}", playerCount);
            cooldownLabelData = new PlayerCooldownLabelData[playerCount];

            for (int i = 0; i < playerCount; i++)
            {
                var labelData = new PlayerCooldownLabelData()
                {
                    itemController = GetPlayerItemController?.Invoke(GameUIRoot.Instance, i)
                };

                var panel = labelData.itemController.GetComponent<dfPanel>();
                var cooldownLabel = panel.AddControl<dfLabel>();
                cooldownLabel.TextScale = 2;
                cooldownLabel.AutoSize = true;
                cooldownLabel.IsVisible = false;
                labelData.cooldownLabel = cooldownLabel;

                if (labelData.itemController.IsRightAligned)
                {
                    // for player 2 in coop
                    labelData.BaseRelativeX = 525;
                    labelData.BaseRelativeX = 510;
                }
                else
                {
                    labelData.BaseRelativeX = 150;
                    labelData.BaseRelativeY = 510;
                }

                labelData.UpdateLabelPostion();

                cooldownLabelData[i] = labelData;
            }
        }

        public void Clear()
        {
            if (cooldownLabelData != null)
            {
                foreach (var labelData in cooldownLabelData)
                {
                    labelData.Destroy();
                }

                cooldownLabelData = null;
            }
        }

        private static Func<GameUIRoot, int, GameUIItemController> GetPlayerItemControllerDelegate()
        {
            var method = typeof(GameUIRoot).GetMethod("GetItemControllerForPlayerID", BindingFlags.NonPublic | BindingFlags.Instance);
            if (method != null)
            {
                var del = Delegate.CreateDelegate(typeof(Func<GameUIRoot, int, GameUIItemController>), method);
                Func<GameUIRoot, int, GameUIItemController> f = (Func<GameUIRoot, int, GameUIItemController>)del;
                return (r, i) =>
                {
                    if (r?.itemControllers == null)
                    {
                        return null;
                    }

                    return f(r, i);
                };
            }
            else
            {
                StaticLogger.LogDebug("Could not find GetItemControllerForPlayerID");
                return (r, i) => null;
            }
        }
    }

    internal class PlayerCooldownLabelData
    {
        public GameUIItemController itemController;

        public dfLabel cooldownLabel;

        public PlayerItem lastItem;

        public bool isGhost;

        public int BaseRelativeX = 0, BaseRelativeY = 0;

        public int AdditionalOffsetPx = 2;

        public dfAnchorStyle Anchor = dfAnchorStyle.Bottom | dfAnchorStyle.Left;

        public dfPivotPoint Pivot = dfPivotPoint.BottomLeft;

        public int lastActiveCount = -1;

        public float lastCooldown;

        public bool showDecimal;

        public bool UpdateBoundState(PlayerController player)
        {
            bool dirty = false;

            if (player.IsGhost != isGhost)
            {
                isGhost = player.IsGhost;
                dirty = true;
            }

            if (player.CurrentItem != lastItem)
            {
                lastItem = player.CurrentItem;
                dirty = true;
            }

            if (lastItem)
            {
                float cooldown;
                showDecimal = false;
                if (lastItem.CurrentRoomCooldown > 0)
                {
                    cooldown = lastItem.CurrentRoomCooldown;
                }
                else if (lastItem.CurrentDamageCooldown > 0)
                {
                    cooldown = (int)lastItem.CurrentDamageCooldown;
                }
                else if (lastItem.CurrentTimeCooldown > 0)
                {
                    showDecimal = true;
                    cooldown = lastItem.CurrentTimeCooldown;
                }
                else
                {
                    cooldown = 0;
                }

                if (cooldown != lastCooldown)
                {
                    lastCooldown = cooldown;
                    dirty = true;
                }
            }

            if (lastActiveCount != player.activeItems.Count)
            {
                lastActiveCount = player.activeItems.Count;
                dirty = true;
            }

            return dirty;
        }

        public void UpdateLabelState()
        {
            if (!lastItem || lastCooldown == 0 || isGhost)
            {
                cooldownLabel.IsVisible = false;
            }
            else
            {
                cooldownLabel.IsVisible = true;
                string format = showDecimal ? "0.0" : "0";
                cooldownLabel.Text = lastCooldown.ToString(format);
                UpdateLabelPostion();
            }
        }

        public void UpdateLabelPostion()
        {
            cooldownLabel.Anchor = Anchor;
            cooldownLabel.Pivot = Pivot;
            cooldownLabel.RelativePosition = new Vector3(BaseRelativeX, BaseRelativeY);
            float offset = AdditionalOffsetPx * Pixelator.Instance.CurrentTileScale * itemController.AdditionalItemBoxSprites.Count;
            if (itemController.IsRightAligned)
                offset = -offset;
            cooldownLabel.RelativePosition = new Vector3(BaseRelativeX + offset, BaseRelativeY);
        }

        public void Destroy()
        {
            if (cooldownLabel)
                UnityEngine.Object.Destroy(cooldownLabel);
        }
    }
}