﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ItemTipsMod
{
    internal class SynergyManagerMonitorBehaviour : MonoBehaviour
    {
        public ItemTipsModule ItemTips;

        private void Update()
        {
            if (ItemTips != null)
            {
                ItemTips.CheckForSynergyManagerChanges();
            }
        }
    }
}
