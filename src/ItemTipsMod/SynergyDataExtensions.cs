﻿namespace ItemTipsMod
{
    internal static class SynergyDataExtensions
    {
        public static string GetLocalizedSynergyName(this SynergyData synergy)
        {
            if (synergy.Key == null)
            {
                return synergy.Name;
            }
            else
            {
                string synergyName = StringTableManager.GetSynergyString(synergy.Key);
                if (string.IsNullOrEmpty(synergyName))
                {
                    StaticLogger.LogDebug($"Did not get localized name for {synergy.Key}");
                    return synergy.Key;
                }
                else
                {
                    return synergyName;
                }
            }
        }
    }
}

