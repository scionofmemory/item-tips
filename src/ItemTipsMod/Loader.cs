﻿using System;
using System.IO;

namespace ItemTipsMod
{
    public static class Loader
    {
        public static string DataFilePrefix { get; } = "itemtips";

        public static string DataFileExtension { get; } = ".tip";

        internal static string SearchPattern { get; } = $"{DataFilePrefix}*{DataFileExtension}";


        public static bool ThrowIfException = DefaultThrowSetting();

        public static PickupAndSynergyDataCache LoadItemData()
        {
            var cache = new PickupAndSynergyDataCache();
            try
            {
                var set = GetInternalDataSet();
                foreach (var item in set.Items)
                {
                    item.SourceMetadata = SourceMetadata.InternalSource;
                    if (item.Id.HasValue)
                    {
                        cache.Pickups[item.Id.GetValueOrDefault()] = item;
                    }
                    else if (item.Ids != null)
                    {
                        foreach (int id in item.Ids)
                        {
                            cache.Pickups[id] = item;
                        }
                    }
                    else
                    {
                        StaticLogger.LogDebug($"Could not process item '{item.Name}' because it does not have an id");
                    }

                    foreach (var synergy in set.Synergies)
                    {
                        synergy.SourceMetadata = SourceMetadata.InternalSource;
                        if (synergy.Key != null && synergy.Key.StartsWith("#"))
                        {
                            cache.Synergies[synergy.Key] = synergy;
                        }
                        else
                        {
                            StaticLogger.LogDebug($"Could not process item '{synergy.Name}' because it does not have a proper id");
                        }
                    }
                }
            }
            catch (Exception e) when (!ThrowIfException)
            {
                StaticLogger.LogInfo($"Error processing item data: {e}");
            }

            return cache;
        }

        public static ExternalDataCache LoadExternalTips(string directory)
        {
            var cache = new ExternalDataCache();
            try
            {
                if (Directory.Exists(directory))
                {
                    int filesRead = 0;
                    foreach (var filePath in Directory.GetFiles(directory, SearchPattern, SearchOption.AllDirectories))
                    {
                        try
                        {
                            filesRead++;
                            ProcessJsonFile(cache, filePath);
                        }
                        catch (Exception e)
                        {
                            StaticLogger.LogError($"Failed to read external file '{filePath}': {e}");
                        }
                    }

                    StaticLogger.LogDebug($"Files processed: {filesRead}");
                }
                else
                {
                    StaticLogger.LogDebug($"Directory does not exist: {directory}");
                }
            }
            catch (Exception e)
            {
                StaticLogger.LogInfo($"Failed to read resource directory '{directory}': {e}");
            }

            return cache;
        }

        internal static InternalDataSet GetInternalDataSet()
        {
            foreach (var name in typeof(ItemTipsModule).Assembly.GetManifestResourceNames())
            {
                if (!name.Contains("data.json"))
                {
                    continue;
                }

                using (var stream = typeof(ItemTipsModule).Assembly.GetManifestResourceStream(name))
                {
                    var set = JsonHelper.DeserializeFromStream<InternalDataSet>(stream);
                    if (set != null && set.Items != null)
                    {
                        return set;
                    }
                    else
                    {
                        StaticLogger.LogDebug($"Found stream {name} but had no data");
                    }
                }
            }

            throw new InvalidOperationException("Could not find internal data set");
        }

        private static void ProcessJsonFile(ExternalDataCache cache, string file)
        {
            StaticLogger.LogInfo($"Reading file: {file}");
            using (var fs = File.OpenRead(file))
            {
                ProcessJsonFromStream(cache, fs, $"file {file}");
            }
        }

        //private static void ProcessZipFile(ExternalDataCache cache, string file)
        //{
        //    StaticLogger.LogDebug($"Reading archive: {file}");
        //    using (var zip = ZipFile.Read(file))
        //    {
        //        foreach (ZipEntry entry in zip.Entries)
        //        {
        //            if (entry.FileName.EndsWith(".json"))
        //            {
        //                StaticLogger.LogDebug($"Reading entry: {entry.FileName}");
        //                using (var ms = new MemoryStream())
        //                {
        //                    entry.Extract(ms);
        //                    ms.Seek(0L, SeekOrigin.Begin);
        //                    ProcessJsonFromStream(cache, ms, $"archive {file}, entry:{entry.FileName}");
        //                }
        //            }
        //        }
        //    }
        //}

        private static void ProcessJsonFromStream(ExternalDataCache cache, Stream jsonStream, string descriptor)
        {
            try
            {
                var set = JsonHelper.DeserializeFromStream<ExternalDataSet>(jsonStream);

                if (set.Metadata != null)
                {
                    StaticLogger.LogDebug($"name:{set.Metadata.Name}, version:{set.Metadata.Version}");
                }

                var setItems = set?.Items;
                if (setItems != null && setItems.Count > 0)
                {
                    foreach (var kvp in setItems)
                    {
                        var data = kvp.Value;
                        data.SourceMetadata = set.Metadata;
                        if (!string.IsNullOrEmpty(data.Notes) && !data.Notes.EndsWith("."))
                        {
                            data.Notes += ".";
                        }

                        cache.Pickups[kvp.Key] = kvp.Value;
                    }
                }

                var setSynergies = set?.Synergies;
                if (setSynergies != null && setSynergies.Count > 0)
                {
                    foreach (var kvp in setSynergies)
                    {
                        var data = kvp.Value;
                        data.SourceMetadata = set.Metadata;
                        if (!string.IsNullOrEmpty(data.Notes) && !data.Notes.EndsWith("."))
                        {
                            data.Notes += ".";
                        }

                        cache.Synergies[kvp.Key] = kvp.Value;
                    }
                }

                if (cache.Pickups.Count == 0 && cache.Synergies.Count == 0)
                {
                    StaticLogger.LogDebug($"Nothing found to load in {descriptor}");
                }
            }
            catch (Exception e)
            {
                StaticLogger.LogError($"Failed to read json from {descriptor}: {e}");
            }
        }

        private static bool DefaultThrowSetting()
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }
    }
}

