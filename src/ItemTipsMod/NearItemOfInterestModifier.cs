﻿using SGUI;

namespace ItemTipsMod
{
    internal class NearItemOfInterestModifier : SModifier
    {
        private readonly ItemTipsModule _module;

        public bool IsNearItem = false;

        public NearItemOfInterestModifier(ItemTipsModule module)
        {
            _module = module;
        }

        public override void Update()
        {
            Elem.Enabled = _module.TipsEnabledConfig.Value;
            
            if (GameManager.Instance.IsPaused)
            {
                Elem.Visible = false;
            }
            else
            {
                Elem.Visible = IsNearItem;
            }
        }
    }
}

