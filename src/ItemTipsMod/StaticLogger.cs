﻿using System;
using System.IO;
using System.Text;
using BepInEx.Logging;

namespace ItemTipsMod
{
    internal static class StaticLogger
    {
        public static ManualLogSource Source;

        public static void LogInfo(string message) => Source?.LogInfo(message);

        public static void LogDebug(string message) => Source?.LogDebug(message);

        public static void LogDebug<T>(string format, T arg) => Source?.LogDebug(string.Format(format, arg));

        public static void LogDebug(string format, params object[] args) => Source?.LogDebug(string.Format(format, args));

        public static void LogError(string format, params object[] args) => Source?.LogError(string.Format(format, args));
    }
}

