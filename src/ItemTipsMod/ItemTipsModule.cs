﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using ETGGUI;
using SGUI;
using UnityEngine;
using BepInEx;
using BepInEx.Configuration;

namespace ItemTipsMod
{
    [BepInDependency(ETGModMainBehaviour.GUID)]
    [BepInPlugin(GUID, NAME, VERSION)]
    public class ItemTipsModule : BaseUnityPlugin
    {
        public const string GUID = "glorfindel.etg.itemtips";
        public const string NAME = "ItemTips";
        public const string VERSION = "2.2.0";

        private const string DefaultSynergyHighlightColor = "32C23A";
        private const string DefaultCoolnessHighlightColor = "7ACAD6";
        private const string DefaultCurseHighlightColor = "745AC7";

        private static readonly Color LabelBackgroundColor = new Color(0, 0, 0, 0.5f);

        public ConfigEntry<bool> TipsEnabledConfig;
        public ConfigEntry<bool> AmmonomiconEnabledConfig;
        public ConfigEntry<string> ExternalDataPathConfig;
        public ConfigEntry<float> TipsPosisionHorizontal;
        public ConfigEntry<float> TipsPositionVertical;

        private bool _lateStartCalled = false;
        private Settings _currentSettings;
        private Font _gameFont;
        private Label _infoLabel;
        private NearItemOfInterestModifier _modifier;
        private HashSet<int> _specialItemIds;
        private PickupAndSynergyDataCache _tipCache;
        private AmmonomiconEntryCache _ammonomiconCache;
        private AdvancedSynergyCache _synergyCache;
        private TipSource? _lastTipSource;
        private PlayerControllerObserver _observer;

        public ItemTipsModule()
        {
            _tipCache = new PickupAndSynergyDataCache();
            _currentSettings = new Settings();
            _synergyCache = new AdvancedSynergyCache();
            _ammonomiconCache = new AmmonomiconEntryCache(_synergyCache, _tipCache);
            _specialItemIds = new HashSet<int>()
            {
                224, // blank
                120, // armor
                73, // half heart
                85, // full heart
                78, // ammo
                600, // partial ammo
                67, // key
            };
        }

        public PickupAndSynergyDataCache GetDataCache()
        {
            return _tipCache;
        }

        public AdvancedSynergyCache GetSynergyCache()
        {
            return _synergyCache;
        }

        public void CheckForSynergyManagerChanges()
        {
            if (_synergyCache.SourceEntries != GameManager.Instance.SynergyManager.synergies)
            {
                StaticLogger.LogDebug("Detected changed source synergies. Reloading...");
                _synergyCache = new AdvancedSynergyCache(GameManager.Instance.SynergyManager.synergies);
                _ammonomiconCache = new AmmonomiconEntryCache(_synergyCache, _tipCache);
            }
        }

        public void Awake()
        {
            TipsEnabledConfig = Config.Bind("Tips", "Enabled", true, "Enables showing HUD tips near items.");
            AmmonomiconEnabledConfig = Config.Bind("Ammonomicon", "Enabled", true, "Enables showing extended data in the ammonomicon.");
            ExternalDataPathConfig = Config.Bind("Data", "External Data Path", "", "Optional path to directory with external data files. Separate multiple directories with ';'.");
            TipsPosisionHorizontal = Config.Bind<float>("Tips", "PosisionHorizontal", 0.01f, "Label's hosizontal position, where 0.0 is leftmost and 1.0 is rightmost.");
            TipsPositionVertical = Config.Bind<float>("Tips", "PositionVertical", 0.2f, "Label's vertical position, where 0.0 is top and 1.0 is bottom.");
        }

        public void Start()
        {
            StaticLogger.Source = Logger;
            ETGModMainBehaviour.WaitForGameManagerStart(GmStart);
        }

        public void GmStart(GameManager gm)
        {
            try
            {
                StaticLogger.LogDebug("Initializing UI");
                _infoLabel = new Label();
                _modifier = new NearItemOfInterestModifier(this);
                SGUIRoot.Main.Children.Add(_infoLabel);
                _infoLabel.Background = LabelBackgroundColor;
                _infoLabel.OnUpdateStyle = (elem) =>
                {
                    elem.Font = LoadFont();
                    ((Label)elem).Reposition(TipsPosisionHorizontal.Value, TipsPositionVertical.Value);
                };
                _infoLabel.With.Add(_modifier);

                StaticLogger.LogDebug("Loading internal data");
                var watch = Stopwatch.StartNew();
                _tipCache = Loader.LoadItemData();
                _synergyCache = new AdvancedSynergyCache(GameManager.Instance.SynergyManager.synergies);
                _ammonomiconCache = new AmmonomiconEntryCache(_synergyCache, _tipCache);
                watch.Stop();
                StaticLogger.LogDebug($"Loaded {_tipCache.Pickups.Count} items in {watch.ElapsedMilliseconds} ms");

                SetupHoooks();

                _observer = ETGModMainBehaviour.Instance.gameObject.AddComponent<PlayerControllerObserver>();
                _observer.EnteredCombat += OnEnteredCombat;

                var monitor = ETGModMainBehaviour.Instance.gameObject.AddComponent<SynergyManagerMonitorBehaviour>();
                monitor.ItemTips = this;

                var cd = ETGModMainBehaviour.Instance.gameObject.AddComponent<CooldownLabelBehavior>();

                StaticLogger.LogDebug("Defining commands");
                ETGModConsole.Commands.AddGroup("itemtips", (_) => HelpCommand(ArrayHelper<string>.Empty));
                var group = ETGModConsole.Commands.GetGroup("itemtips");
                group.AddUnit("help", HelpCommand);
                group.AddUnit("reload", ReloadCommand);
                group.AddUnit("generate", GenerateSampleCommand);
                DebugHelper.AddDebugCommands(this);

                ETGModConsole.Log($"ItemTips v{VERSION} loaded");
            }
            catch (Exception e)
            {
                StaticLogger.LogError($"ItemTips v{VERSION} Error: {e}");
                ETGModConsole.Log($"ItemTips v{VERSION} Error: {e}");
            }
        }

        private void SetupHoooks()
        {
            StaticLogger.LogDebug("Generating hooks");
            HookHelper.CreateHook(typeof(Gun), nameof(Gun.OnEnteredRange), this, nameof(GunOnEnteredRangeHook));
            HookHelper.CreateHook(typeof(Gun), nameof(Gun.OnExitRange), this, nameof(GunOnExitRangeHook));
            HookHelper.CreateHook(typeof(Gun), nameof(Gun.Pickup), this, nameof(GunPickupHook));

            HookHelper.CreateHook(typeof(PassiveItem), nameof(PassiveItem.OnEnteredRange), this, nameof(PassiveItemOnEnteredRangeHook));
            HookHelper.CreateHook(typeof(PassiveItem), nameof(PassiveItem.OnExitRange), this, nameof(PassiveItemOnExitRangeHook));
            HookHelper.CreateHook(typeof(PassiveItem), nameof(PassiveItem.Pickup), this, nameof(PassiveItemPickupHook));

            // bc the map HAD to be difficult :/
            // since the gungeon map does not call its base in pickup the normal hook doesn't work.
            HookHelper.CreateHook(typeof(GungeonMapItem), nameof(GungeonMapItem.Pickup), this, nameof(PassiveItemPickupHook));

            HookHelper.CreateHook(typeof(PlayerItem), nameof(PlayerItem.OnEnteredRange), this, nameof(ActiveItemOnEnteredRangeHook));
            HookHelper.CreateHook(typeof(PlayerItem), nameof(PlayerItem.OnExitRange), this, nameof(ActiveItemOnExitRangeHook));
            HookHelper.CreateHook(typeof(PlayerItem), nameof(PlayerItem.Pickup), this, nameof(ActiveItemPickupHook));

            HookHelper.CreateHook(typeof(ShopItemController), nameof(ShopItemController.OnEnteredRange), this, nameof(ShopItemOnEnteredRangeHook));
            HookHelper.CreateHook(typeof(ShopItemController), nameof(ShopItemController.OnExitRange), this, nameof(ShopItemOnExitRangeHook));
            HookHelper.CreateHook(typeof(ShopItemController), nameof(ShopItemController.Interact), this, nameof(ShopItemInteractHook));

            HookHelper.CreateHook(typeof(RewardPedestal), nameof(RewardPedestal.OnEnteredRange), this, nameof(RewardPedestalOnEnteredRangeHook));
            HookHelper.CreateHook(typeof(RewardPedestal), nameof(RewardPedestal.OnExitRange), this, nameof(RewardPedestalOnExitRangeHook));
            HookHelper.CreateHook(typeof(RewardPedestal), nameof(RewardPedestal.Interact), this, nameof(RewardPedestalInteractHook));

            // to support "prediction" items and synergies (e.g. the exotic)
            HookHelper.CreateHook(typeof(Chest), nameof(Chest.PredictContents), this, nameof(ChestPredictContentsHook));
            HookHelper.CreateHook(typeof(HologramDoer), nameof(HologramDoer.HideSprite), this, nameof(HologramDoerHideSpriteHook));
            HookHelper.CreateHook(typeof(PlayerController), "OnGunChanged", this, nameof(OnGunChangedHook));

            HookHelper.CreateHook(typeof(Foyer), "Awake", this, nameof(LateStartAwakeHook));

            // for ammonomicon support
            HookHelper.CreateHook(typeof(EncounterDatabaseEntry), nameof(EncounterDatabaseEntry.GetModifiedLongDescription), this, nameof(GetModifiedLongDescriptionHook));

            // TODO: Shrines... some day...
            //HookHelper.CreateHook(typeof(AdvancedShrineController), nameof(AdvancedShrineController.OnEnteredRange), this, nameof(ShrineOnEnteredRangeHook));
            //HookHelper.CreateHook(typeof(AdvancedShrineController), nameof(AdvancedShrineController.OnExitRange), this, nameof(ShrineOnExitRangeHook));
        }

        private void LateStartAwakeHook(Action<Foyer> orig, Foyer self)
        {
            orig(self);

            if (!_lateStartCalled)
            {
                _lateStartCalled = true;
                try
                {
                    ScanExternalData();

                    CheckForSynergyManagerChanges();
                }
                catch (Exception e)
                {
                    StaticLogger.LogError($"Error automatically loading external files: {e}");
                }
            }
        }

        private int ScanExternalData()
        {
            int totalCount = 0;

            {
                var stopwatch = Stopwatch.StartNew();
                int count = _tipCache.LoadExternalDirectory(Paths.PluginPath);
                StaticLogger.LogDebug($"Finished loading external data in BepInEx path . Loaded {count} items. Elapsed: {stopwatch.ElapsedMilliseconds} ms");
                totalCount += count;
            }

            string externalDirectory = ExternalDataPathConfig.Value;
            if (!string.IsNullOrEmpty(externalDirectory))
            {
                string[] directories = externalDirectory.Split(';');
                foreach (var directory in directories)
                {
                    StaticLogger.LogInfo($"Loading external directory: {directory}");
                    var stopwatch = Stopwatch.StartNew();
                    int count = _tipCache.LoadExternalDirectory(directory);
                    StaticLogger.LogDebug($"Finished loading external data in '{directory}'. Loaded {count} items. Elapsed: {stopwatch.ElapsedMilliseconds} ms");
                    totalCount += count;
                }
            }

            return totalCount;
        }

        private void HologramDoerHideSpriteHook(Action<HologramDoer, GameObject, bool> orig, HologramDoer self, GameObject gameObject, bool instant)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, gameObject, instant);
        }

        private void OnEnteredCombat()
        {
            HideTip();
        }

        private string GetModifiedLongDescriptionHook(Func<EncounterDatabaseEntry, string> orig, EncounterDatabaseEntry self)
        {
            string origValue = orig(self);
            if (AmmonomiconEnabledConfig.Value)
            {
                try
                {
                    _ammonomiconCache.SetCacheLanguage(GameManager.Options.CurrentLanguage);
                    int pickupId = self.pickupObjectId;
                    return _ammonomiconCache.GetEntry(pickupId, origValue);
                }
                catch (Exception e)
                {
                    StaticLogger.LogError($"Error in {nameof(GetModifiedLongDescriptionHook)}: {e}");
                }
            }

            return origValue;
        }

        private void HelpCommand(string[] args)
        {
            if (args.Length > 0)
            {
                string commandName = args[0].ToLowerInvariant();
                if (commandName == "reload")
                {
                    ETGModConsole.Log($"Reloads external item description files");
                }
                else if (commandName == "generate")
                {
                    ETGModConsole.Log("Generates a sample external item description file");
                }
                else if (commandName == "help")
                {
                    ETGModConsole.Log("Help generates help");
                }
                else
                {
                    ETGModConsole.Log($"Unknown command {commandName}");
                }

                return;
            }

            ETGModConsole.Log($"ItemTips v{VERSION} command help");
            ETGModConsole.Log($"itemtips reload - Reload external tip files");
            ETGModConsole.Log($"itemtips generate - Create sample external tip file");
            ETGModConsole.Log($"itemtips help [command] - Additional help about the command");
        }

        private void GenerateSampleCommand(string[] args)
        {
            var delimiters = new char[] { ':' };
            string sampleFilePath = null;
            try
            {
                var includePrefix = new HashSet<string>(args ?? ArrayHelper<string>.Empty);

                var externalData = new ExternalDataSet()
                {
                    Metadata = new ExternalMetadata()
                    {
                        Name = "",
                        Url = "",
                        Version = ""
                    },
                    Items = new Dictionary<string, ExternalPickupData>(),
                    Synergies = new Dictionary<string, ExternalSynergyData>()
                };

                foreach (var kvp in Gungeon.Game.Items.Pairs)
                {
                    if (string.IsNullOrEmpty(kvp.Key) ||
                        _specialItemIds.Contains(kvp.Value.PickupObjectId))
                    {
                        continue;
                    }

                    string key = kvp.Key;
                    bool gungeonPrimary = false;
                    string prefix;
                    if (key.StartsWith("gungeon:"))
                    {
                        prefix = "gungeon";
                        if (key.Contains("_dupe"))
                        {
                            continue;
                        }

                        if (key.Contains("+") && key != "gungeon:+1_bullets")
                        {
                            continue;
                        }

                        key = key.Substring("gungeon:".Length);
                        gungeonPrimary = true;
                    }
                    else
                    {
                        string[] items = key.Split(delimiters);
                        prefix = items[0];
                    }

                    string notes = null;
                    if (_tipCache.Pickups.TryGetValue(kvp.Value.PickupObjectId, out var data))
                    {
                        notes = data.Notes;
                    }
                    else if (gungeonPrimary)
                    {
                        // keeps out unused items and other weird stuff
                        // but we still want modded items to show up.
                        continue;
                    }

                    if (includePrefix.Count > 0)
                    {
                        if (!includePrefix.Contains(prefix))
                        {
                            continue;
                        }
                    }

                    externalData.Items[key] = new ExternalPickupData()
                    {
                        Name = kvp.Value.EncounterNameOrDisplayName,
                        Notes = notes ?? string.Empty
                    };
                }

                foreach (var kvp in _synergyCache.SynergyKeyIndex)
                {
                    string notes = null;
                    if (_tipCache.Synergies.TryGetValue(kvp.Value.NameKey, out var data))
                    {
                        notes = data.Effect;
                    }
                    else
                    {
                        // there are no custom synergies that i know of
                        // keeps out unused synergies and other weird stuff
                        continue;
                    }

                    string synergyName = StringTableManager.GetSynergyString(kvp.Key);

                    externalData.Synergies[kvp.Key] = new ExternalSynergyData()
                    {
                        Name = synergyName,
                        Notes = notes ?? string.Empty,
                    };
                }


                string fileName = $"{Loader.DataFilePrefix}-sample{Loader.DataFileExtension}";
                string directory = Environment.GetEnvironmentVariable("HOME") ?? Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                sampleFilePath = Path.Combine(directory, fileName);
                JsonHelper.SerializeToFile(sampleFilePath, externalData);

                long len = new FileInfo(sampleFilePath).Length;

                StaticLogger.LogInfo($"Wrote {len} bytes to to {sampleFilePath}");
                ETGModConsole.Log($"Wrote sample file to {sampleFilePath}");
            }
            catch (Exception e)
            {
                if (sampleFilePath != null)
                {
                    StaticLogger.LogError($"Error creating sample file: '{sampleFilePath}' {e}");
                    ETGModConsole.Log($"Error creating sample file: '{sampleFilePath}' {e}");
                }
                else
                {
                    StaticLogger.LogError($"Error creating sample file: {e}");
                    ETGModConsole.Log($"Error creating sample file: {e}");
                }
            }
        }

        private void ReloadCommand(string[] args)
        {
            int loaded = ScanExternalData();
            ETGModConsole.Log($"ItemTips reloaded {loaded} items");
        }

        private Font LoadFont()
        {
            if (_gameFont == null)
            {
                var gameFont = (dfFont)GameUIRoot.Instance.Manager.DefaultFont;
                _gameFont = FontConverter.GetFontFromdfFont(gameFont, 2);
            }

            return _gameFont;
        }

        private void GunOnEnteredRangeHook(Action<Gun, PlayerController> orig, Gun self, PlayerController player)
        {
            try
            {
                if (TipsEnabledConfig.Value)
                {
                    ShowTip(TipSource.Gun, self.PickupObjectId);
                }
            }
            catch (Exception e)
            {
                StaticLogger.LogDebug($"Error in {nameof(GunOnEnteredRangeHook)}: {e}");
            }

            orig(self, player);
        }

        private void GunOnExitRangeHook(Action<Gun, PlayerController> orig, Gun self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void GunPickupHook(Action<Gun, PlayerController> orig, Gun self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void PassiveItemOnEnteredRangeHook(Action<PassiveItem, PlayerController> orig, PassiveItem self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
            {
                ShowTip(TipSource.PassiveItem, self.PickupObjectId);
            }

            orig(self, player);
        }

        private void PassiveItemOnExitRangeHook(Action<PassiveItem, PlayerController> orig, PassiveItem self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void PassiveItemPickupHook(Action<PassiveItem, PlayerController> orig, PassiveItem self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void ActiveItemOnEnteredRangeHook(Action<PlayerItem, PlayerController> orig, PlayerItem self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
            {
                ShowTip(TipSource.ActiveItem, self.PickupObjectId);
            }

            orig(self, player);
        }

        private void ActiveItemOnExitRangeHook(Action<PlayerItem, PlayerController> orig, PlayerItem self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void ActiveItemPickupHook(Action<PlayerItem, PlayerController> orig, PlayerItem self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void ShopItemOnEnteredRangeHook(Action<ShopItemController, PlayerController> orig, ShopItemController self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
            {
                if (self.item)
                {
                    int pickupId;
                    if (self.CurrencyType == ShopItemController.ShopCurrencyType.META_CURRENCY)
                    {
                        var realPickup = PickupObjectDatabase.GetByEncounterName(self.item.EncounterNameOrDisplayName);
                        pickupId = realPickup?.PickupObjectId ?? -1;
                    }
                    else
                    {
                        pickupId = self.item.PickupObjectId;
                    }

                    ShowTip(TipSource.ShopItem, pickupId);
                }
            }

            orig(self, player);
        }

        private void ShopItemOnExitRangeHook(Action<ShopItemController, PlayerController> orig, ShopItemController self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void ShopItemInteractHook(Action<ShopItemController, PlayerController> orig, ShopItemController self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void RewardPedestalOnEnteredRangeHook(Action<RewardPedestal, PlayerController> orig, RewardPedestal self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
            {
                int pickupId = self.contents?.PickupObjectId ?? -1;
                ShowTip(TipSource.RewardPedestal, pickupId);
            }

            orig(self, player);
        }

        private void RewardPedestalOnExitRangeHook(Action<RewardPedestal, PlayerController> orig, RewardPedestal self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private void RewardPedestalInteractHook(Action<RewardPedestal, PlayerController> orig, RewardPedestal self, PlayerController player)
        {
            if (TipsEnabledConfig.Value)
                HideTip();
            orig(self, player);
        }

        private List<PickupObject> ChestPredictContentsHook(Func<Chest, PlayerController, List<PickupObject>> orig, Chest self, PlayerController player)
        {
            var pickups = orig(self, player);
            if (pickups.Count > 0 && TipsEnabledConfig.Value)
                ShowTip(TipSource.ChestPredicted, pickups[0].PickupObjectId);
            return pickups;
        }

        private void OnGunChangedHook(Action<PlayerController, Gun, Gun, Gun, Gun, bool> orig, PlayerController self, Gun previous, Gun current, Gun previousSecondary, Gun currentSecondary, bool newGun)
        {
            if (TipsEnabledConfig.Value)
            {
                // handles the odd case where they are near a predicted chest and swap guns.
                if (_lastTipSource == TipSource.ChestPredicted)
                {
                    HideTip();
                }
            }

            orig(self, previous, current, previousSecondary, currentSecondary, newGun);
        }

        private void ShrineOnEnteredRangeHook(Action<AdvancedShrineController, PlayerController> orig, AdvancedShrineController self, PlayerController player)
        {
            orig(self, player);
        }

        private void ShrineOnExitRangeHook(Action<AdvancedShrineController, PlayerController> orig, AdvancedShrineController self, PlayerController player)
        {
            orig(self, player);
        }

        private void ShowTip(TipSource source, int pickupId)
        {
            if (pickupId < 0)
            {
                _modifier.IsNearItem = false;
                _lastTipSource = null;
                return;
            }

            try
            {
                if (_specialItemIds.Contains(pickupId))
                {
                    _modifier.IsNearItem = false;
                    _lastTipSource = null;
                    return;
                }

                var pickup = PickupObjectDatabase.GetById(pickupId);
                if (!_tipCache.Pickups.TryGetValue(pickupId, out var data))
                {
                    data = null;
                }

                var textInfo = GenerateTextInfo(pickup, data);
                _infoLabel.Text = textInfo.LabelText;
                _infoLabel.Size = textInfo.LabelSize;
                _modifier.IsNearItem = true;
                _lastTipSource = source;
                _infoLabel.Reposition(TipsPosisionHorizontal.Value, TipsPositionVertical.Value);
            }
            catch (Exception e)
            {
                StaticLogger.LogDebug($"ShowTip Error {source} '{pickupId}': {e}");
            }
        }

        private void HideTip()
        {
            _modifier.IsNearItem = false;
            _lastTipSource = null;
        }

        private TextInfo GenerateTextInfo(PickupObject pickup, ItemData item)
        {
            var lines = new List<string>()
            {
                pickup.EncounterNameOrDisplayName
            };

            TipBuilder.AppendQualityDescriptor(pickup, line => lines.Add(line));

            var entry = EncounterDatabase.GetEntry(pickup.encounterTrackable.EncounterGuid);
            if (entry != null)
            {
                string typeOrStyle = entry.GetSecondTapeDescriptor();
                if (entry.shootStyleInt >= 0)
                {
                    lines.Add($"Style: {typeOrStyle}");
                }
                else if (entry.isPassiveItem || entry.isPlayerItem)
                {
                    lines.Add($"Type: {typeOrStyle}");
                }
            }

            TipBuilder.AppendActiveCooldownType(pickup, line => lines.Add(line));

            if (item != null)
            {
                if (!string.IsNullOrEmpty(item.Notes))
                {
                    var noteLines = ConvertStringToFixedWidthLines(item.Notes, _currentSettings.LineWidth);
                    lines.AddRange(noteLines);
                }
            }

            TipBuilder.AppendModifiers(pickup, (stat, line) =>
            {
                string colorString = null;
                switch (stat)
                {
                    case PlayerStats.StatType.Coolness:
                        colorString = DefaultCoolnessHighlightColor;
                        break;
                    case PlayerStats.StatType.Curse:
                        colorString = DefaultCurseHighlightColor;
                        break;
                }

                if (string.IsNullOrEmpty(colorString))
                {
                    lines.Add(line);
                }
                else
                {
                    lines.Add($"<color=#{colorString}>{line}</color>");
                }
            });

            var handledSynergies = new HashSet<string>();
            if (_synergyCache.ItemIndex.TryGetValue(pickup.PickupObjectId, out var synergyList))
            {
                if (synergyList.Count > 0)
                {
                    lines.Add("Synergies:");

                    // 'real' synergies
                    foreach (var synergy in synergyList)
                    {
                        string synergyName = StringTableManager.GetSynergyString(synergy.NameKey);
                        if (!handledSynergies.Add(synergyName))
                            continue;

                        string effect = null;
                        bool wouldGetSynergy = synergy.SynergyIsAvailable(GameManager.Instance.PrimaryPlayer, GameManager.Instance.SecondaryPlayer, pickup.PickupObjectId);
                        if (wouldGetSynergy && _tipCache.Synergies.TryGetValue(synergy.NameKey, out var synergyData))
                        {
                            effect = synergyData.Effect;
                        }

                        if (wouldGetSynergy)
                        {
                            if (!string.IsNullOrEmpty(effect))
                            {
                                lines.Add($" - <color=#{DefaultSynergyHighlightColor}>{synergyName}</color>:");
                                var effectLines = ConvertStringToFixedWidthLines(effect, _currentSettings.LineWidth);
                                lines.AddRange(effectLines);
                            }
                            else
                            {
                                lines.Add($" - <color=#{DefaultSynergyHighlightColor}>{synergyName}</color>");
                            }
                        }
                        else
                        {
                            lines.Add($" - {synergyName}");
                        }
                    }
                }
            }

            if (item != null)
            {
                if (item.Synergies.Length > 0)
                {
                    if (handledSynergies.Count == 0)
                    {
                        lines.Add("Synergies:");
                    }

                    // psuedo synergies like Cormorant
                    foreach (var synergy in item.Synergies)
                    {
                        string synergyName = synergy.GetLocalizedSynergyName();
                        if (!handledSynergies.Add(synergyName))
                            continue;

                        // prevent duplicate synergies details
                        if (synergy.Key != null && handledSynergies.Contains(synergy.Key))
                            continue;

                        string effect = null;

                        bool wouldGetSynergy = synergy.InternalWouldGetSynergy();
                        if (wouldGetSynergy)
                        {
                            effect = synergy.Effect;
                        }

                        if (wouldGetSynergy)
                        {
                            if (!string.IsNullOrEmpty(effect))
                            {
                                lines.Add($" - <color=#{DefaultSynergyHighlightColor}>{synergyName}</color>:");
                                var effectLines = ConvertStringToFixedWidthLines(effect, _currentSettings.LineWidth);
                                lines.AddRange(effectLines);
                            }
                            else
                            {
                                lines.Add($" - <color=#{DefaultSynergyHighlightColor}>{synergyName}</color>");
                            }
                        }
                        else
                        {
                            lines.Add($" - {synergyName}");
                        }
                    }
                }
            }

            var size = _currentSettings.GetSize(lines.Count);
            string text = string.Join("\n", lines.ToArray());

            return new TextInfo(text, size);
        }

        private static List<string> ConvertStringToFixedWidthLines(string text, int lineWidth)
        {
            var lines = new List<string>();
            if (text.Length < lineWidth)
            {
                lines.Add(text);
            }
            else
            {
                var words = text.Split();
                var builder = new StringBuilder();
                foreach (var word in words)
                {
                    if (builder.Length + word.Length + 1 >= lineWidth)
                    {
                        lines.Add(builder.ToString());
                        builder.Length = 0;
                        builder.Append(word);
                    }
                    else
                    {
                        if (builder.Length > 0)
                        {
                            builder.Append(" ");
                        }

                        builder.Append(word);
                    }
                }

                if (builder.Length > 0)
                {
                    lines.Add(builder.ToString());
                }
            }

            return lines;
        }
    }
}

