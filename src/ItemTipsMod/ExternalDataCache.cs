﻿using System.Collections.Generic;

namespace ItemTipsMod
{
    public class ExternalDataCache
    {
        public Dictionary<string, ExternalPickupData> Pickups = new Dictionary<string, ExternalPickupData>();
        public Dictionary<string, ExternalSynergyData> Synergies = new Dictionary<string, ExternalSynergyData>();
    }
}

