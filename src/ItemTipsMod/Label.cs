﻿using SGUI;
using UnityEngine;

namespace ItemTipsMod
{
    internal class Label : SLabel
    {
        public Label()
        {
        }

        public void Reposition(float leftFraction, float topFraction)
        {
            if (Root != null)
            {
                Position.x = Root.Size.x * leftFraction;
                Position.y = Root.Size.y * topFraction;
            }
        }
    }
}

