Pre-BepInEx changes:

version 1.3.0
* Cooldown indicator: shows damage/time/rooms needed for active item cooldown to expire.
* Load external tips from zip files in the ext folder.

version 1.2.2
* Work better with cross-mod synergies. Detect added synergies after loading.

version 1.2.1
* Don't show synergies with empty names (caused by certain mods, not the base game).
* Additional description for Ruby Bracelet.

version 1.2.0
* Adds info on source of external items to ammonomicon entry.
* Fixes showing unused/cut synergies.
* Fixes issues with some custom synergies.
* Fixes issue with 'Smart Bombs' synergy not displaying correctly.

version 1.1.2
* Fixed issue with map tip not disappearing after pickup.
* Added indicator in ammonomicon for active synergies on an item/gun.
* Support custom synergies. Added several external description files as addons.

version 1.1.1
* Fix incorrect Gunderfury description.

version 1.1
* Fixed issue with some ammonomicon entries not appearing correctly.
* Fixes to work better with modded items.
* Fixes to work better with non-English language settings.
* Show synergy description when it will be activated in tip.
* Add / override builtin descriptions .
* Add commands: config, reload, generate, help.

version 1.0
* Tips for items and guns in-game.
* Expanded ammonomicon entries.
